package co.com.uniminuto.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import co.com.uniminuto.user.model.Client;
import co.com.uniminuto.user.service.ClientService;

@Controller
public class ClientController {
	
	@Autowired
	ClientService clientService;
	
    @GetMapping("/")
    public String viewHomePage(Model model) {
        model.addAttribute("listClients", clientService.getAllClients());
        return "index";
    }	
    
    @GetMapping("/showNewClientForm")
    public String showNewClientForm(Model model) {
        Client client = new Client();
        model.addAttribute("client", client);
        return "new_client";
    }
    
    @PostMapping("/saveClient")
    public String saveClient(@ModelAttribute("client") Client client) {
        clientService.saveClient(client);
        return "redirect:/";
    }
    
    @GetMapping("/showFormForUpdate/{id}")
    public String showFormForUpdate(@PathVariable(value = "id") long id, Model model) {
        Client client = clientService.getClientById(id);
        model.addAttribute("client", client);
        return "update_client";
    }    
    
    @GetMapping("/deleteClient/{id}")
    public String deleteClient(@PathVariable(value = "id") long id) {
        this.clientService.deleteClientById(id);
        return "redirect:/";
    }    

}
