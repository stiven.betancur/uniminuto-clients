package co.com.uniminuto.user.service;

import java.util.List;

import co.com.uniminuto.user.model.Client;

public interface ClientService {
    List < Client > getAllClients();
    void saveClient(Client client);
    Client getClientById(long id);
    void deleteClientById(long id);
}