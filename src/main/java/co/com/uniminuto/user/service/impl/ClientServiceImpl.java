package co.com.uniminuto.user.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.uniminuto.user.model.Client;
import co.com.uniminuto.user.repository.ClientRepository;
import co.com.uniminuto.user.service.ClientService;

@Service
public class ClientServiceImpl implements ClientService {
	
	@Autowired
	ClientRepository clientRepository;

	@Override
	public List<Client> getAllClients() {
		return clientRepository.findAll();
	}

	@Override
	public void saveClient(Client client) {
		this.clientRepository.save(client);
	}

	@Override
	public Client getClientById(long id) {
        Optional < Client > optional = clientRepository.findById(id);
        Client client = null;
        if (optional.isPresent()) {
        	client = optional.get();
        } else {
            throw new RuntimeException(" Client not found for id :: " + id);
        }
        return client;
	}

	@Override
	public void deleteClientById(long id) {
		this.clientRepository.deleteById(id);
	}

}
