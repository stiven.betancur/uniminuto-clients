package co.com.uniminuto.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.com.uniminuto.user.model.Client;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long>{

}
